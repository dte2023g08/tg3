import nltk #Importamos la herramienta NLTK
import time #Importamos el módulo time para poder medir el tiemp de ejecución de cada una de las funciones
nltk.download('punkt') #Descargamos los datos necesarios
nltk.download('averaged_perceptron_tagger') #Se descarga lo necesario para el tagging
nltk.download('treebank') #Se descarga lo necesario para el análisis stáctico
nltk.download('wordnet') #Se descarga lo necesario para lematización

from nltk.tokenize import word_tokenize, sent_tokenize #Se importa lo necesario para tokenizar

texto = "The Republic is crumbling under attacks by the ruthless Sith Lord, Count Dooku. There are heroes on both sides. Evil is everywhere. In a stunning move, the fiendish droid leader, General Grievous, has swept into the Republic capital and kidnapped Chancellor Palpatine, leader of the Galactic Senate." #Texto utilizado para todo el desarrollo
#Tokenización: separa el texto en palabras
inicioToken = time.time() #Inicia el temporizador
tokens = nltk.word_tokenize(texto) #Realiza la tokenizacion

finToken = time.time() #Finaliza el temporizador
tiempo_ejecucionToken = finToken - inicioToken #Calcula el tiempo
print(tokens) #Muestra el resultado de la tokenización
print("Tiempo de ejecución de la tokkenización: ", tiempo_ejecucionToken) #Muestra el tiempo calculado anteriormente

#Tagging: es el proceso de asignar etiquetas a las palabras en un texto según su categoría gramatical
inicioTagg = time.time() #Inicia el temporizador
tagged = nltk.pos_tag(tokens) #Realiza la acción del tagging
finTagg = time.time() #Finaliza el temporizador
tiempo_ejecucionTagg = finTagg - inicioTagg #Calcula el tiempo
print(tagged) #Muestra el resultado del tagging
print("Tiempo de ejecución del tagging: ", tiempo_ejecucionTagg ) #Muestra el tiempo calculado anteriormente

#Stemming: reducción a la raíz útil en modelos que busquen palabras similares, de correr o corriendo saca corr
inicioStem = time.time() #Inicia el temporizador
from nltk.stem import PorterStemmer #Importa lo necesario para que se pueda realizar el stemming
stemmer = PorterStemmer() #crea una variable
for palabra in tokens: #busca en un bucle for
    print(stemmer.stem(palabra)) #muestra el resultado del stemming
finStem = time.time() #Finaliza el temporizador
tiempo_ejecucionStemm = finStem - inicioStem #Calcula el tiempo
print("Tiempo de ejecución del Steamming: ", tiempo_ejecucionStemm ) #Muestra el tiempo calculado anteriormente

#Lematización: reducción al lema, similar al stemming pero mas preciso de corriendo saca correr o de fue saca ser, útil en modelos de traducción
inicioLem = time.time() #Inicia el temporizador
from nltk.stem import WordNetLemmatizer #Importa lo necesario para la lematización
lemmatizer = WordNetLemmatizer() #crea una variable
lemmatized_tokens = [] #crea una lista vacía
for token in tokens: #realiza un bucle for
    lemmatized_tokens.append(lemmatizer.lemmatize(token)) #Realiza la lematización
finLem = time.time() #Finaliza el temporizador
print(lemmatized_tokens) #Muestra el resultado de la lematización
tiempo_ejecucionLem = finLem - inicioLem #Calcula el tiempo
print("Tiempo de ejecución de la lematización: ", tiempo_ejecucionLem ) #Muestra el tiempo calculado anteriormente